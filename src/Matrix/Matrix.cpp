#include "Matrix.h"
#include <immintrin.h>

using namespace std;




Matrix::Matrix(int height, int width)
{
    this->height = height;
    this->width = width;
    this->array = std::vector<std::vector<float> >(height, std::vector<float>(width));
}

Matrix::Matrix(std::vector<std::vector<float> > const &array)
{
    assert(array.size()!=0);
    this->height = array.size();
    this->width = array[0].size();
    this->array = array;
}

Matrix::Matrix(){}

int Matrix::getHeight()
{
    return height;
}

int Matrix::getWidth()
{
    return width;
}

void Matrix::fill(float const &value)
{
    int i,j;
    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            array[i][j] = value;
        }
    }
}

void Matrix::put(int h, int w, float const &value)
{
    array[h][w] = value;
}

float Matrix::get(int h, int w) const
{
    return array[h][w];
}

Matrix Matrix::add(float const &value)
{
    int i,j;
    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            array[i][j] += value;
        }
    }

    return *this;
}

Matrix Matrix::add_simd(float const &value)
{
    /*
    // Sečtení 8 floatů s konstantou naráz
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec; // vektor 8 floatů
    __m256 float_value = _mm256_broadcast_ss(&value); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec = _mm256_load_ps(array[i].data() + j);
            float_vec = _mm256_add_ps(float_vec, float_value);
            _mm256_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek sečte normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
            array[i][j] += value;
        }
    }*/

    // Sečtení 4 floaty s konstantou naráz
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    __m128 float_vec; // vektor čtyř floatů
    __m128 float_value = _mm_load_ss(&value);
    float_value = _mm_shuffle_ps(float_value, float_value, 0); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec = _mm_load_ps(array[i].data() + j);
            float_vec = _mm_add_ps(float_vec, float_value);
            _mm_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek sečte normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            array[i][j] += value;
        }
    }

    return *this;
}

Matrix Matrix::subtract(float const &value)
{
    int i,j;
    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            array[i][j] -= value;
        }
    }

    return *this;
}

Matrix Matrix::subtract_simd(float const &value)
{
    /*
    // Odečtení 8 floatů s konstantou naráz
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec; // vektor 8 floatů
    __m256 float_value = _mm256_broadcast_ss(&value); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec = _mm256_load_ps(array[i].data() + j);
            float_vec = _mm256_sub_ps(float_vec, float_value);
            _mm256_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek odečte normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
            array[i][j] -= value;
        }
    }*/

    // Odecte od 4 floatů konstantu naráz
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    __m128 float_vec; // vektor čtyř floatů
    __m128 float_value = _mm_load_ss(&value);
    float_value = _mm_shuffle_ps(float_value, float_value, 0); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec = _mm_load_ps(array[i].data() + j);
            float_vec = _mm_sub_ps(float_vec, float_value);
            _mm_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek odečte normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            array[i][j] -= value;
        }
    }

    return *this;
}

Matrix Matrix::multiply(float const &value)
{
    int i,j;
    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            array[i][j] *= value;
        }
    }
    return *this;
}

Matrix Matrix::multiply_simd(float const &value)
{
    /*
    // Vynásobý 8 floatů s konstantou naráz
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec; // vektor 8 floatů
    __m256 float_value = _mm256_broadcast_ss(&value); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec = _mm256_load_ps(array[i].data() + j);
            float_vec = _mm256_mul_ps(float_vec, float_value);
            _mm256_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek vynásový normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
            array[i][j] *= value;
        }
    }*/

     // Vynásobý 4 floaty s konstantou naráz
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    __m128 float_vec; // vektor čtyř floatů
    __m128 float_value = _mm_load_ss(&value);
    float_value = _mm_shuffle_ps(float_value, float_value, 0); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec = _mm_load_ps(array[i].data() + j);
            float_vec = _mm_mul_ps(float_vec, float_value);
            _mm_store_ps(array[i].data() + j, float_vec);
        }

        // Zbytek vynásobý normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            array[i][j] *= value;
        }
    }

    return *this;
}

Matrix Matrix::add(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    f_matrix();
    Matrix result(height, width);
    int i,j;

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            result.array[i][j] = array[i][j] + m.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::add_simd(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    //f_matrix();
    Matrix result(height, width);

    /*
    // Sečtení 8 floatů
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec_a; 
    __m256 float_vec_b;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec_a = _mm256_load_ps(array[i].data() + j);
            float_vec_b = _mm256_load_ps(m.array[i].data() + j);
            float_vec_a = _mm256_add_ps(float_vec_a, float_vec_b);
            _mm256_store_ps(result.array[i].data() + j, float_vec_a);
        }

        // Zbytek sečte normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
           result.array[i][j] = array[i][j] + m.array[i][j];
        }
    }*/

     // Sečtě dva vektery o čtyřech floatech
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a; 
    __m128 float_vec_b;
    __m128 float_vec_res;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec_a = _mm_load_ps(array[i].data() + j);
            float_vec_b = _mm_load_ps(m.array[i].data() + j);
            float_vec_res = _mm_add_ps(float_vec_a, float_vec_b);
            _mm_store_ps(result.array[i].data() + j, float_vec_res);
        }

        // Zbytek sečte normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] + m.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::subtract(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    Matrix result(height, width);
    int i,j;

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            result.array[i][j] = array[i][j] - m.array[i][j];
        }
    }
    return result;
}

Matrix Matrix::subtract_simd(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    Matrix result(height, width);

    /*
    // Odečtení 8 floatů
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec_a; 
    __m256 float_vec_b;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec_a = _mm256_load_ps(array[i].data() + j);
            float_vec_b = _mm256_load_ps((m.array[i]).data() + j);
            float_vec_a = _mm256_sub_ps(float_vec_a, float_vec_b);
            _mm256_store_ps((result.array[i]).data() + j, float_vec_a);
        }

        // Zbytek odečte normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] - m.array[i][j];
        }
    }*/

    // Odečte dva vektery o čtyřech floatech
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a; 
    __m128 float_vec_b;
    __m128 float_vec_res;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec_a = _mm_load_ps(array[i].data() + j);
            float_vec_b = _mm_load_ps(m.array[i].data() + j);
            float_vec_res = _mm_sub_ps(float_vec_a, float_vec_b);
            _mm_store_ps(result.array[i].data() + j, float_vec_res);
        }

        // Zbytek odečte normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] - m.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::multiply(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    Matrix result(height, width);
    int i,j;

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            result.array[i][j] = array[i][j] * m.array[i][j];
        }
    }
    return result;
}

Matrix Matrix::multiply_simd(Matrix const &m) const
{
    assert(height==m.height && width==m.width);

    Matrix result(height, width);

    /*
    // Násobení 8 floatů
    int do_together = width / 8; // float se osekne na int, zaokrouhlení dolů

    __m256 float_vec_a; 
    __m256 float_vec_b;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*8 ; j += 8)
        {
            float_vec_a = _mm256_load_ps(array[i].data() + j);
            float_vec_b = _mm256_load_ps((m.array[i]).data() + j);
            float_vec_a = _mm256_mul_ps(float_vec_a, float_vec_b);
            _mm256_store_ps((result.array[i]).data() + j, float_vec_a);
        }

        // Zbytek odečte normálně
        for (int j = do_together*8 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] * m.array[i][j];
        }
    }*/

    // Vynásobý dva vektery o čtyřech floatech
    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a; 
    __m128 float_vec_b;
    __m128 float_vec_res;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec_a = _mm_load_ps(array[i].data() + j);
            float_vec_b = _mm_load_ps(m.array[i].data() + j);
            float_vec_res = _mm_mul_ps(float_vec_a, float_vec_b);
            _mm_store_ps(result.array[i].data() + j, float_vec_res);
        }

        // Zbytek vynásobý normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] * m.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::dot(Matrix const &m) const
{
    assert(width==m.height);

    int i,j,h, mwidth = m.width;
    float w=0;

    Matrix result(height, mwidth);

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<mwidth ; j++)
        {
            for (h=0 ; h<width ; h++)
            {
                w += array[i][h]*m.array[h][j];
            }
            result.array[i][j] = w;
            w=0;
        }
    }

    return result;
}

Matrix Matrix::dot_simd(Matrix const &m) const
{
    // Nejede 
    assert(width==m.height);

    int mwidth = m.width;
    float w=0;

    Matrix result(height, mwidth);

    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a;
    __m128 float_vec_b;
    __m128 float_vec_res; 
    
    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < mwidth ; j++)
        {
            for (int h = 0; h < do_together*4 ; h += 4)
            {
                // Načtení více čísel naráz a ty pak naráz sečíst
                float_vec_a = _mm_load_ps(array[i].data() + h);
                float tmp_vec[4] = {m.array[h][j], m.array[h+1][j], m.array[h+2][j], m.array[h+3][j]};
                float_vec_b = _mm_load_ps(tmp_vec);
                float_vec_res = _mm_mul_ps(float_vec_a, float_vec_b);

                // Horizontální sečtení
                __m128 shuf   = _mm_shuffle_ps(float_vec_res, float_vec_res, _MM_SHUFFLE(2, 3, 0, 1));  // [ C D | A B ]
                __m128 sums   = _mm_add_ps(float_vec_res, shuf);      // sums = [ D+C C+D | B+A A+B ]
                shuf          = _mm_movehl_ps(shuf, sums);      //  [   C   D | D+C C+D ]  // let the compiler avoid a mov by reusing shuf
                sums          = _mm_add_ss(sums, shuf);
                w += _mm_cvtss_f32(sums);
            }

            // Zbytek vynásobý normálně
            for (int h = do_together*4 ; h < width ; h++)
            {
                w += array[i][h]*m.array[h][j];
            }

            result.array[i][j] = w;
            w=0;
        }
    }

    return result;
}

Matrix Matrix::transpose() const
{
    Matrix result(width, height);
    int i,j;

    for (i=0 ; i<width ; i++){
        for (j=0 ; j<height ; j++){
            result.array[i][j] = array[j][i];
        }
    }
    return result;
}

Matrix Matrix::transpose_simd() const
{
    // Moc to nezrychluje ale buďiš
    Matrix result(width, height);

    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec; 
    
    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            // Načtení víc floatů naráz a pak uložení po jednom do sloupců
            float_vec = _mm_load_ps(array[i].data() + j);
            _MM_EXTRACT_FLOAT(result.array[j][i], float_vec, 0);
            _MM_EXTRACT_FLOAT(result.array[j+1][i], float_vec, 1);
            _MM_EXTRACT_FLOAT(result.array[j+2][i], float_vec, 2);
            _MM_EXTRACT_FLOAT(result.array[j+3][i], float_vec, 3);
        }

        // Zbytek vynásobý normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[j][i] = array[i][j];
        }
    }

    return result;
}

Matrix Matrix::subtract_and_multiply_simd(Matrix const &a, Matrix const &b) const
{
    assert(height==a.height && width==a.width);
    assert(height==b.height && width==b.width);

    Matrix result(height, width);

    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a; 
    __m128 float_vec_b;
    __m128 float_vec_c;
    __m128 float_vec_res;
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec_a = _mm_load_ps(array[i].data() + j);
            float_vec_b = _mm_load_ps(a.array[i].data() + j);
            float_vec_c = _mm_load_ps(b.array[i].data() + j);

            // Prvně odečíst pak vynásobyt
            float_vec_res = _mm_sub_ps(float_vec_a, float_vec_b);
            float_vec_res = _mm_mul_ps(float_vec_res, float_vec_c);

            _mm_store_ps(result.array[i].data() + j, float_vec_res);
        }

        // Zbytek udělá normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[i][j] = array[i][j] - a.array[i][j];
            result.array[i][j] = result.array[i][j] * b.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::multiply_and_subtract_simd(Matrix const &a, float const &value) const
{
    assert(height==a.height && width==a.width);

    Matrix result(height, width);

    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a; 
    __m128 float_vec_b;
    __m128 float_vec_res;

    __m128 float_value = _mm_load_ss(&value);
    float_value = _mm_shuffle_ps(float_value, float_value, 0); // distribude hodnoty do všech položek vektoru
    

    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < do_together*4 ; j += 4)
        {
            float_vec_a = _mm_load_ps(array[i].data() + j);
            float_vec_b = _mm_load_ps(a.array[i].data() + j);

            // Prvně vynásobit a * value pak odečíst o matice
            float_vec_res = _mm_mul_ps(float_vec_b, float_value);
            float_vec_res = _mm_sub_ps(float_vec_a, float_vec_res);

            _mm_store_ps(result.array[i].data() + j, float_vec_res);
        }

        // Zbytek udělá normálně
        for (int j = do_together*4 ; j < width ; j++)
        {
            result.array[i][j] = a.array[i][j] * value;
            result.array[i][j] = array[i][j] - result.array[i][j];
        }
    }

    return result;
}

Matrix Matrix::dot_transpose_simd(Matrix const &m) const
{
    assert(width==m.width);

    int mheight = m.height;
    float w=0;

    Matrix result(height, mheight);

    int do_together = width / 4; // float se osekne na int, zaokrouhlení dolů

    // vektory čtyř floatů
    __m128 float_vec_a;
    __m128 float_vec_b;
    __m128 float_vec_res; 
    
    for (int i = 0 ; i < height ; i++)
    {
        for (int j = 0; j < mheight ; j++)
        {
            for (int h = 0; h < do_together*4 ; h += 4)
            {
                // Načtení více čísel naráz a ty pak naráz sečíst
                float_vec_a = _mm_load_ps(array[i].data() + h);
                float_vec_b = _mm_load_ps(m.array[j].data() + h);
                float_vec_res = _mm_mul_ps(float_vec_a, float_vec_b);

                // Přičtení
                // WTF nefunguje
                //float_vec_res = _mm_hadd_ps(float_vec_res, float_vec_res);
                //float_vec_res = _mm_hadd_ps(float_vec_res, float_vec_res);
                float t;
                _MM_EXTRACT_FLOAT(t, float_vec_res, 0);
                w += t;
                _MM_EXTRACT_FLOAT(t, float_vec_res, 1);
                w += t;
                _MM_EXTRACT_FLOAT(t, float_vec_res, 2);
                w += t;
                _MM_EXTRACT_FLOAT(t, float_vec_res, 3);
                w += t;
            }

            // Zbytek vynásobý normálně
            for (int h = do_together*4 ; h < width ; h++)
            {
                w += array[i][h]*m.array[j][h];
            }

            result.array[i][j] = w;
            w=0;
        }
    }

    return result;
}

Matrix Matrix::transpose_dot_simd(Matrix const &m) const
{
    // Specializováno jen pro dvě funkce které to volají
    Matrix result(width, m.width);

    // vektory čtyř floatů
    __m128 float_vec_a;
    __m128 float_vec_b;
    __m128 float_vec_res;

    int do_together = m.width / 4;

    for (int i=0 ; i<width ; i++)
    {
        float_vec_a = _mm_load_ss(&array[0][i]);
        float_vec_a = _mm_shuffle_ps(float_vec_a, float_vec_a, 0);

        for (int j = 0; j < do_together*4; j+=4)
        {
            float_vec_b = _mm_load_ps(m.array[0].data() + j);
            float_vec_res = _mm_mul_ps(float_vec_a, float_vec_b);

            // Uložení
            _MM_EXTRACT_FLOAT(result.array[i][j], float_vec_res, 0);
            _MM_EXTRACT_FLOAT(result.array[i][j+1], float_vec_res, 1);
            _MM_EXTRACT_FLOAT(result.array[i][j+2], float_vec_res, 2);
            _MM_EXTRACT_FLOAT(result.array[i][j+3], float_vec_res, 3);
        }

        for (int j = do_together*4; j < m.width; j++)
        {
            result.array[i][j] = m.array[0][j] * array[0][i];
        }
    }

    return result;
}

Matrix Matrix::applyFunction(float (*function)(float)) const
{
    Matrix result(height, width);
    int i,j;

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++){
            float in=this->array[i][j];
            float x= (*function)(in);
            result.array[i][j] = x;
        }
    }

    return result;
}

Matrix Matrix::subMatrix(int startH, int startW, int h, int w) const
{
    assert(startH+h<=height && startW+w<=width);

    Matrix result(h,w);
    int i,j;

    for (i=startH ; i<startH+h ; i++)
    {
        for (j=startW ; j<startW+w ; j++)
        {
            result.array[i-startH][j-startW] = array[i][j];
        }
    }
    return result;
}

void Matrix::print(std::ostream &flux) const
{
    int i,j;
    int maxLength[width] = {};
    std::stringstream ss;

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            ss << array[i][j];
            if(maxLength[j] < ss.str().size())
            {
                maxLength[j] = ss.str().size();
            }
            ss.str(std::string());
        }
    }

    for (i=0 ; i<height ; i++)
    {
        for (j=0 ; j<width ; j++)
        {
            flux << array[i][j];
            ss << array[i][j];
            for (int k=0 ; k<maxLength[j]-ss.str().size()+1 ; k++)
            {
                flux << " ";
            }
            ss.str(std::string());
        }
        flux << std::endl;
    }
}

bool Matrix::operator==(Matrix const &m)
{
    if(height==m.height && width==m.width)
    {
        int i,j;
        for (i=0 ; i<height ; i++)
        {
            for (j=0 ; j<width ; j++)
            {
                if(array[i][j]!=m.array[i][j])
                {
                    return false;
                }
            }
        }
        return true;
    }
    return false;
}

bool Matrix::operator!=(Matrix const &m)
{
    return !operator==(m);
}

void Matrix::operator+=(Matrix const &m)
{
    this->array = add(m).array;
}

void Matrix::operator-=(Matrix const &m)
{
    this->array = subtract(m).array;
}

void Matrix::operator*=(Matrix const &m)
{
    this->array = multiply(m).array;
}

void Matrix::operator+=(float const &m)
{
    add(m);
}

void Matrix::operator-=(float const &m)
{
    subtract(m);
}

void Matrix::operator*=(float const &m)
{
    multiply(m);
}

float& Matrix::operator()(int y, int x)
{
    assert(y<height && x<width);
    return array[y][x];
}

Matrix operator+(Matrix const &a, Matrix const &b)
{
    return a.add(b);
}

Matrix operator-(Matrix const &a, Matrix const &b)
{
    return a.subtract(b);
}

Matrix operator*(Matrix const &a, Matrix const &b)
{
    return a.multiply(b);
}

Matrix operator+(Matrix const &a, float const &b)
{
    Matrix result = a;
    return result.add(b);
}

Matrix operator-(Matrix const &a, float const &b)
{
    Matrix result = a;
    return result.subtract(b);
}

Matrix operator*(Matrix const &a, float const &b)
{
    Matrix result = a;
    return result.multiply(b);
}

std::ostream& operator<<(std::ostream &flux, Matrix const &m)
{
    m.print(flux);
    return flux;
}